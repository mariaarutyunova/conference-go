from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": f"downtown {city} {state}",
    }
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_weather_data(city, state):
    url = "https://api.openweathermap.org/geo/1.0/direct"
    params = {"q": f"{city}, {state}, US", "appid": OPEN_WEATHER_API_KEY}
    response = requests.get(url, params=params)
    response = json.loads(response.content)
    lat = response[0]["lat"]
    lon = response[0]["lon"]

    url_current = "https://api.openweathermap.org/data/2.5/weather"
    params_current = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    response_current = requests.get(url_current, params=params_current)
    current_weather = json.loads(response_current.content)
    weather = {
        "temp": current_weather["main"]["temp"],
        "description": current_weather["weather"][0]["description"],
    }
    return weather
